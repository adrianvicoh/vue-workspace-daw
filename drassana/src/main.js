import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.js";
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
//import { getAnalytics } from "firebase/analytics";
import { getDatabase } from "firebase/database";
//import { getFirestore } from 'firebase/firestore/lite';

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyC26Jwinf4VgTYszlBwRbdodwG9mCtu-CM",
  authDomain: "drassana-bootstrap-i-fir-aaf83.firebaseapp.com",
  projectId: "drassana-bootstrap-i-fir-aaf83",
  storageBucket: "drassana-bootstrap-i-fir-aaf83.appspot.com",
  messagingSenderId: "240561805488",
  appId: "1:240561805488:web:b77b922cf1386ebcf80375",
  measurementId: "G-LS22LL8MWF",
  databaseURL: "https://drassana-bootstrap-i-fir-aaf83-default-rtdb.europe-west1.firebasedatabase.app/"
};

// Initialize Firebase
const appFirebase = initializeApp(firebaseConfig);
//const analytics = getAnalytics(appFirebase);
//console.log(analytics);

export const dbFirebaseRealTime = getDatabase(appFirebase);

createApp(App).use(store).use(router).mount('#app');