import { createStore } from 'vuex'
import * as carregarNaus from '../actions/carregarXMLNaus.js'
import * as carregarTripulants from '../actions/carregarXMLTripulants.js'
import * as Firebase from './../../apiFirebase/index.js'
import { Ordenacio } from './../llibreries/ordenacio.js'
//import * as carregarTripulants from '../actions/carregarXMLTripulants.js'

export default createStore({
  state: {
    nausCarregades:[],
    tripulantsCarregats:[],
    nausDeBDFirebase: [],
    tripulantsDeBDFirebase: []
  },
  getters: {
    getDadesNau: (state) => (filtreNauId) => {
      let dadesNau = state.nausCarregades.filter(function(nauTmp){
        if (nauTmp.id == filtreNauId) {
          return true;
        } else {
          return false;
        }
      });
        console.log(dadesNau);
        return dadesNau[0];
    },
    getDadesTripulant: (state) => (filtreTripulantNom) => {
      let dadesTripulant = state.tripulantsDeBDFirebase.filter(function(tripulantTmp){
        if (tripulantTmp.nom == filtreTripulantNom) {
          return true;
        } else {
          return false;
        }
      });
        console.log(dadesTripulant);
        return dadesTripulant[0];
    },
    getNausDelFirebase (state) {
      // sort(): ordena els elements de l'array i retorna l'array ordenat (modifica l'array original).
      // Retornarem totes les naus però ordenades ppel ID:
      return state.nausDeBDFirebase.sort(Ordenacio.ordenarAlfabeticamentNauNom);
    }
  },
  mutations: {
    mutationCrearNau(state, dadesNau) {
      state.nausCarregades.push(dadesNau);
    },
    mutationCrearTripulant(state, dadesTripulant) {
      state.tripulantsCarregats.push(dadesTripulant);
    },
    mutationInsertarNauDEBFirebase(state, dada) {
      state.nausDeBDFirebase.push(dada);
    },
    mutationInsertarNauDeBDFirebaseVsTonelatge(state, dada) {
      state.nausDeBDFirebase.push(dada);
    },
    mutationInsertarTripulantDEBFirebase(state, dada) {
      state.tripulantsDeBDFirebase.push(dada);
    },
    mutationEliminarTripulantDEBFirebase(state, nom) {
      const index = state.tripulantsDeBDFirebase.findIndex(function(tripulant) {
        return tripulant.nom = nom;
      });
      if (index !== -1) {
        state.tripulantsDeBDFirebase.splice(index, 1);
      }
    },
    mutationModificarTripulantDEBFirebase(state, newData) {
      const index = state.tripulantsDeBDFirebase.findIndex(function(tripulant) {
        return tripulant.nom = newData.nom;
      });
      if (index !== -1) {
        state.tripulantsDeBDFirebase[index].cognom = newData.cognom;
        state.tripulantsDeBDFirebase[index].mare = newData.mare;
        state.tripulantsDeBDFirebase[index].rang = newData.rang;
        state.tripulantsDeBDFirebase[index].afilicacio = newData.afiliacio;
        state.tripulantsDeBDFirebase[index].ocupacio = newData.ocupacio;
        state.tripulantsDeBDFirebase[index].nacionalitat = newData.nacionalitat;
      }
    }
  },
  actions: {
    /*
    El parámetro store se le puede llamar como se quiera,
    siempre se entenderá como que el primer parámetro,
    si no está definido, es el store
    */
    funcioCarregaNaus(store) {
      carregarNaus.carregarXMLNaus('xmls/naus_Star_Trek.xml', store);
    },
    funcioCarregaTripulants(store) {
      carregarTripulants.carregarXMLTripulants('xmls/tripulants_Star_Trek.xml', store);
    },
    carregarNausDeBDFirebase(context) {
      Firebase.getNausDelFirebase(context);
    },
    carregarNausDeBDFirebaseVstonelatge(context, tonelatgeMinim, tonelatgeMaxim) {
      Firebase.getNausDelFirebaseVSTonelatge(context, tonelatgeMinim, tonelatgeMaxim);
    },
    carregarTripulantsDeBDFirebase(context) {
      Firebase.getTripulantsDelFirebase(context);
    },
    eliminarTripulantDeBDFirebase(context, nom) {
      Firebase.eliminarTripulantDelFirebase(context, nom);
    },
    modificarTripulantDeBDFirebase(context, nom, newData) {
      Firebase.modificarTripulantDelFirebase(context, nom, newData);
    },
  },
  modules: {
  }
})
