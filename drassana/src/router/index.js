import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/nausView',
    name: 'nausView',
    component: () => import('../views/nausView.vue'),
    children: [
        {
          path: '/llistaNausView',
          name: 'llistaNausView',
          component: () => import('../views/llistaNausView.vue')
        },
        {
          path: '/altaNauView',
          name: 'altaNauView',
          component: () => import('../views/altaNauView.vue')
        },
        {
          path: '/modificaNauView',
          name: 'modificaNauView',
          component: () => import('../views/modificaNauView.vue')
        },
        {
          path: '/veureDadesNau/:id/:nom',
          name: 'veureDadesNau',
          component: () => import('../views/veureDadesNau.vue'),
          props: true         // Si no posem això no podrem rebre paràmetres.
        }
    ]
  },
  {
    path: '/tripulantsView',
    name: 'tripulantsView',
    component: () => import('../views/tripulantsView.vue'),
    children: [
        {
          path: '/llistaTripulantsView',
          name: 'llistaTripulantsView',
          component: () => import('../views/llistaTripulantsView.vue')
        },
        {
          path: '/altaTripulantView',
          name: 'altaTripulantView',
          component: () => import('../views/altaTripulantView.vue')
        },
        {
          path: '/modificaTripulantView',
          name: 'modificaTripulantView',
          component: () => import('../views/modificaTripulantView.vue')
        },
        {
          path: '/veureDadesTripulant/:id/:nom',
          name: 'veureDadesTripulant',
          component: () => import('../views/veureDadesTripulant.vue'),
          props: true         // Si no posem això no podrem rebre paràmetres.
        },
        {
          path: '/editarTripulant/:nom',
          name: 'editarTripulant',
          component: () => import('../views/editarTripulant.vue'),
          props: true
        },
    ]
  },
  {
    path: '/reparacionsView',
    name: 'reparacionsView',
    component: () => import('../views/reparacionsView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
