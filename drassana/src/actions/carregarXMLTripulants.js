import * as Firebase from "./../../apiFirebase/index.js";

export function carregarXMLTripulants(nomFitxer, /*context*/) {
    const opcions = {
        method: "GET"
    }

    // Demanem els fitxers XML al servidor:
    fetch(nomFitxer, opcions)
        .then(respostaDelFetch => {
            // Si rebem el fitxer del server fem:
            if (respostaDelFetch.ok) {
                return respostaDelFetch.text();
            } else {
                throw new Error(respostaDelFetch.status)
            }
        })
        .then(function(dades){
            // Si s'ha executat la promesa anterior fem:
            alert("dades = " + dades);

            // Ara processem el XML i fiquem les seves dades en la store.
            const parseador = new DOMParser();
            const xmlDoc = parseador.parseFromString(dades, "text/xml");
            const tripulants = xmlDoc.getElementsByTagName("tripulant");

            alert("num de tripulants = " + tripulants.length);

            let dadesTripulant = {};

            for(let i = 0; i < tripulants.length; i++) {
                /*let conjuges = [];
                for(let i = 0; i < tripulants[i].getElementsByTagName("conjuges")[0].length; i++) {
                    conjuges.push(tripulants[i].getElementsByTagName("conjugues")[0][i].textContent);
                }*/
                dadesTripulant = {
                    nom: tripulants[i].getElementsByTagName("nom")[0].textContent,
                    cognom: tripulants[i].getElementsByTagName("cognom")[0].textContent,
                    //conjuges: conjuges,
                    mare: tripulants[i].getElementsByTagName("mare")[0].textContent,
                    rang: tripulants[i].getElementsByTagName("rang")[0].textContent,
                    afiliacio: tripulants[i].getElementsByTagName("afiliacio")[0].textContent,
                    ocupacio: tripulants[i].getElementsByTagName("principal")[0].textContent,
                    nacionalitat: tripulants[i].getElementsByTagName("nacionalitat")[0].textContent,
                    imatge: tripulants[i].getElementsByTagName("imatge")[0].textContent,
                    web: tripulants[i].getElementsByTagName("web")[0].textContent
                };
                // Enviem la nova tripulant a la store.
                // context.commit('mutationCrearTripulant', dadesTripulant);
                // Enviem la nova tripulant a la BD remota del Firebase
                Firebase.insertarTripulantEnFirebase(dadesTripulant);
            }
        });
    }