import * as Firebase from "./../../apiFirebase/index.js";

export function carregarXMLNaus(nomFitxer, context) {
    const opcions = {
        method: "GET"
    }

    // Demanem els fitxers XML al servidor:
    fetch(nomFitxer, opcions)
        .then(respostaDelFetch => {
            // Si rebem el fitxer del server fem:
            if (respostaDelFetch.ok) {
                return respostaDelFetch.text();
            } else {
                throw new Error(respostaDelFetch.status)
            }
        })
        .then(function(dades){
            // Si s'ha executat la promesa anterior fem:
            alert("dades = " + dades);

            // Ara processem el XML i fiquem les seves dades en la store.
            const parseador = new DOMParser();
            const xmlDoc = parseador.parseFromString(dades, "text/xml");
            const naus = xmlDoc.getElementsByTagName("nau");

            alert("num de naus = " + naus.length);

            let dadesNau = {};
            for(let i = 0; i < naus.length; i++) {
                dadesNau = {
                    nom: naus[i].getElementsByTagName("nom")[0].textContent,
                    id: naus[i].getElementsByTagName("id")[0].textContent,
                    classe: naus[i].getElementsByTagName("classe")[0].textContent,
                    tipus: naus[i].getElementsByTagName("tipus")[0].textContent,
                    tonelatge: naus[i].getElementsByTagName("tonelatge")[0].textContent,
                    tripulacio_standart: naus[i].getElementsByTagName("tripulacio_standart")[0].textContent,
                    limit_d_evacuacio: naus[i].getElementsByTagName("limit_d_evacuacio")[0].textContent,
                    fasers_quantitat: naus[i].getElementsByTagName("fasers_quantitat")[0].textContent,
                    fasers_potencia: naus[i].getElementsByTagName("fasers_potencia")[0].textContent,
                    torpedes_quantitat_tubs: naus[i].getElementsByTagName("torpedes_quantitat_tubs")[0].textContent,
                    torpedes_emmagatzemats: naus[i].getElementsByTagName("torpedes_emmagatzemats")[0].textContent,
                    potencia_escuts: naus[i].getElementsByTagName("potencia_escuts")[0].textContent,
                    potencia_escuts_unitat: naus[i].getElementsByTagName("potencia_escuts")[0].getAttribute("unitat"),
                    velocitat_normal_de_creuer: naus[i].getElementsByTagName("velocitat_normal_de_creuer")[0].textContent,
                    velocitat_maxima_de_creuer: naus[i].getElementsByTagName("velocitat_maxima_de_creuer")[0].textContent,
                    velocitat_maxima: naus[i].getElementsByTagName("velocitat_maxima")[0].textContent,
                    temps_de_vida_del_casc: naus[i].getElementsByTagName("temps_de_vida_del_casc")[0].textContent,
                    temps_de_vida_del_casc_unitat: naus[i].getElementsByTagName("temps_de_vida_del_casc")[0].getAttribute("unitat"),
                    imatge: naus[i].getElementsByTagName("imatge")[0].textContent,
                    pag_web: naus[i].getElementsByTagName("pag_web")[0].textContent,
                    quantitat_construides: naus[i].getElementsByTagName("quantitat_construides")[0].textContent,
                    quantitat_construides_destruides: naus[i].getElementsByTagName("quantitat_construides")[0].getAttribute("destruides")
                };

                //console.log(dadesNau);

                // Enviem la nova nau a la store.
                context.commit('mutationCrearNau', dadesNau);

                // Enviem la nova nau a la BD remota del Firebase
                Firebase.insertarNauEnFirebase(dadesNau);
            }
        });
    }