import { dbFirebaseRealTime } from "./../src/main.js";
import { onValue, query, ref, set, startAt, endAt,  orderByChild, get, remove } from "firebase/database";

export function insertarNauEnFirebase(dadesNau) {
    console.log("datos = " + dbFirebaseRealTime);
    set(ref(dbFirebaseRealTime, 'Star_Trek/naus/' + dadesNau.id), {
        nom: dadesNau.nom,
        id: dadesNau.id,
        classe: dadesNau.classe,
        tipus: dadesNau.tipus,
        tonelatge: parseInt(dadesNau.tonelatge),
        tripulacio_standart: parseInt(dadesNau.tripulacio_standart),
        limit_d_evacuacio: parseInt(dadesNau.limit_d_evacuacio),
        fasers_quantitat: parseInt(dadesNau.fasers_quantitat),
        fasers_potencia: dadesNau.fasers_potencia,
        torpedes_quantitat_tubs: parseInt(dadesNau.torpedes_quantitat_tubs),
        torpedes_emmagatzemats: parseInt(dadesNau.torpedes_emmagatzemats),
        potencia_escuts: parseInt(dadesNau.potencia_escuts),
        potencia_escuts_unitat: dadesNau.potencia_escuts_unitat,
        velocitat_normal_de_creuer: parseInt(dadesNau.velocitat_normal_de_creuer),
        velocitat_maxima_de_creuer: parseInt(dadesNau.velocitat_maxima_de_creuer),
        velocitat_maxima: dadesNau.velocitat_maxima,
        temps_de_vida_del_casc: parseInt(dadesNau.temps_de_vida_del_casc),
        temps_de_vida_del_casc_unitat: dadesNau.temps_de_vida_del_casc_unitat,
        imatge: dadesNau.imatge,
        pag_web: dadesNau.pag_web,
        quantitat_construides: parseInt(dadesNau.quantitat_construides),
        quantitat_construides_destruides: parseInt(dadesNau.quantitat_construides_destruides)
    });
}

export function getNausDelFirebase(context) {
    // Creem una referéncia a la ruta 'star Trek/naus/':
    const itemsRef = ref(dbFirebaseRealTime, "star Trek/naus/");
    console.log("itemsRef = " + itemsRef);

    // Per llegir dades d'una ruta (en el nostre cas 'Star Trek/naus/') es fa servir onValue()
    // i li passem per paràmetre la referència a la ruta (la variable itemsRef).
    // Retorna una instantànea (snapshot) que conté totes les dades de la ruta donada.
    // Podem accedir a les dades fent snapshot.val() però no serveix per a res perquè ens
    // interessarà recorrer les dades per agafar les naus 1a 1.
    // Si no hi hagués dades a llavors la snapshot donarà false si cridem a exists() i
    // null quan cridem a val()
    onValue(itemsRef, (snapshot) => {
        console.log("snapshot = " + snapshot);
        console.log("snapshot.exist() = " + snapshot.exists());
        console.log("snapshot.val() = " + snapshot.val());

        // Recorren les dades de la instantanea (snapshot té totes les naus) i per a cadascuna
        // d'elles (la variable childsnapshot correspon a la nau per la que estem recorrent)
        // la enviem a la propietat nausDeBDFirebase del state de la store fent servir la
        // mutation mutationInsertarNauDEBDFirebase:
        snapshot.forEach((childSnapshot) => {
            // Recordar que les dades es guarden en format clau : valor.
            console.log ('childsnapshot.key: ', childSnapshot.key);
            console.log('childsnapshot.val(): ', childSnapshot.val());

            // Enviem la nova nau a la store fent servir una mutation.
            context.commit('mutationInsertarNauDEBDFirebase', childSnapshot.val());
        });
    });       
}

export function getNausDelFirebaseVSTonelatge(context, tonelatgeMinim, tonelatgeMaxim) {
    // Agafarà de la BD del Firebase totes les naus que estiguin entre un mínim i un màxim
    // de tonelatge
    // Si des de la vista cridrem a aquesta funció i fem que retorni una array amb les naus,
    // tenim dificultats perquè sigui reactiu en la vista però si fiquem les dades en una
    // propietat del state de la store i la visualitzem sí que funciona la reactivitat sense
    // problemes.
    
    /*
    Ens ha sortit l'error:
    
    Uncaught (in promise) Error: Index not defined, add ".indexOn": "tonelatge",
    for path "/Star Trek/naus", to the rules
    
    Aquest error ens ha sortit perque volem ordernar (orderBychild()) per "tonelatge".
    
    Per arreglar-lo modifiquem les "Reglas del Firebase" (anar a la consola del Firebase i anar
    a la Realtime Database --> pestanya Reglas) per a fer un index per "tonelatge"
    
        {
            "rules": {
                "read": true,
                "write": true,
                "star Trek": {
                    "naus": {
                        ".indexOnt: "tonelatge"

                    }
                }
            }
        }
    
    Més informació en:
    | https://firebase.google.com/docs/database/security/indexing-data?hl=es-419
    */

    /*
    Informació sobre busquedes i filtres
    https: //firebase.google.con/docs/database/web/lists-of -data?hl=es-419#sorting and filtering data
    https: //firebase.google.com/docs/database/adnin/retrieve-datazhl=es-419#range-queries
    https: //firebase.google.com/docs/reference/js/v8/firebase. database. Query#methods 1
    */

    // Creem una referéncia a la ruta 'star Trek/naus/' de la nostra BD (una variable reactiva):
    const itemsRef = ref(dbFirebaseRealTime, 'Star Trek/naus');

    // Creem una consulta a la base de dades fent servir el métode query(). Aquesta consulta
    // ordenará el resultat segons la propietat "tonelatge" (és un tag dins del JSON de la BD)
    // gracies al orderByChild() i només ens retornará les naus que compleixin les condicions
    // marcades en starAt() i endAt().

    // starAt() defineix el límit inferior a la búsqueda i endat() el superior. Tots 2 es

    // es refereixen a la propietat indicada en el orderByChild().

    const filteredItemsRef = query(
        itemsRef,
        orderByChild("tonelatge"),
        startAt(parseInt(tonelatgeMinim)),	// 1r filtre: agafa les naus que tonelatge >= tonelatgeMinim.
        endAt(parseInt(tonelatgeMaxim))	    // 2n filtre: agafa les naus que tonelatge <= tonelatgeMaxim.
        //equalTo(tonelatgeMinin)			// Filtre que agafaria les naus amb tonelatge == tonelatgeMinim.
    );

    // Per llegir dades d'una ruta (en el nostre cas 'Star Trek/naus/') es fa servir onValue()
    // i li passem per paràmetre la referència a la ruta (la variable itemsRef) + el filtre (aquestes
    // 2 dades estan en la variable filtereditensRef la qual és una query()).
    // Retorna una instantánea (snapshot) que conté totes les dades filtrades de la ruta donada.
    onValue(filteredItemsRef, (snapshot) => {
        console.log("1111 snapshot = " + snapshot);
        console.log("1111 snapshot.exist() = " + snapshot.exists());
        console.log("1111 snapshot.size = " + snapshot.size);
        console.log("1111 snapshot.val() = " + snapshot.val());

        // Agafem els resultats de 1 en 1.
        snapshot.forEach((childSnapshot) => {
            console.log("1111 nauTmp.id = " + childSnapshot.key + " (tonelatge = " + childSnapshot.val().tonelatge + ")");
            context.commit('mutationInsertarNauDeBDFirebaseVsTonelatge', childSnapshot.val());
        });
    });

}

export function insertarTripulantEnFirebase(dadesTripulant) {
    console.log("datos = " + dbFirebaseRealTime);
    set(ref(dbFirebaseRealTime, 'Star_Trek/tripulants/' + dadesTripulant.nom), {
        nom: dadesTripulant.nom,
        cognom: dadesTripulant.cognom,
        mare: dadesTripulant.mare,
        rang: dadesTripulant.rang,
        afiliacio: dadesTripulant.afiliacio,
        ocupacio: dadesTripulant.ocupacio,
        nacionalitat: dadesTripulant.nacionalitat,
        imatge: dadesTripulant.imatge,
        web: dadesTripulant.web
    });
}

export function getTripulantsDelFirebase(context) {
    const tripulants = ref(dbFirebaseRealTime, 'Star_Trek/tripulants/');

    onValue(tripulants, (coleccion) => {
        // console.log("Colección encontrada: ", coleccion);

        coleccion.forEach((element) => {
            context.commit('mutationInsertarTripulantDEBFirebase', element.val());
        });
    });
}

export function eliminarTripulantDelFirebase(context, nom) {
    console.log(context, nom);
    const tripulantsRef = ref(dbFirebaseRealTime, 'Star_Trek/tripulants/');

    get(tripulantsRef)
        .then((snapshot) => {
            snapshot.forEach((childSnapshot) => {
                const tripulant = childSnapshot.val();
                if (tripulant.nom === nom) {
                    // Eliminar el objeto de la base de datos de Firebase
                    remove(childSnapshot.ref)
                        .then(() => {
                            console.log('Objeto eliminado de Firebase');
                        })
                        .catch((error) => {
                            console.error('Error al eliminar objeto de Firebase:', error);
                        });
                }
            });
        })
        .catch((error) => {
            console.error('Error al obtener datos de Firebase:', error);
        });
}

export function modificarTripulantDelFirebase(context, newData) {
    const tripulantRef = ref(dbFirebaseRealTime, `Star_Trek/tripulants/${newData.nom}`);
    set(tripulantRef, newData)
        .then(() => {
            console.log("Tripulant modificado en Firebase");
            tripulantRef.cognom = newData.cognom;
            tripulantRef.mare = newData.mare;
            tripulantRef.rang = newData.rang;
            tripulantRef.afiliacio = newData.afilicacio;
            tripulantRef.ocupacio = newData.ocupacio;
            tripulantRef.nacionalitat = newData.nacionalitat;
        })
        .catch((error) => {
            console.error("Error al modificar tripulant en Firebase:", error);
        });
}